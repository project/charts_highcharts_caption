CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* How it works
* Maintainers


INTRODUCTION
------------

This module creates a Views area plugin (for the View footer) that gets added
inside the chart as the "caption".

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/charts_highcharts_caption

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/search/charts_highcharts_caption


REQUIREMENTS
------------

Views, Charts, and Charts Highcharts must be installed, and the chart in
question must be created by a View, with the chart library set to Highcharts.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


HOW IT WORKS
------------

After you have created a Chart in Views, you can add a "Chart Caption" to the
Views footer. This will add a caption to the chart. The caption can use HTML.

MAINTAINERS
-----------

Current maintainers:
* Daniel Cothran (andileco) - https://www.drupal.org/u/andileco
