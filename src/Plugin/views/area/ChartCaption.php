<?php

namespace Drupal\charts_highcharts_caption\Plugin\views\area;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\TextCustom;

/**
 * Provides a custom text area with chart captions.
 *
 * @ViewsArea("chart_caption")
 */
class ChartCaption extends TextCustom {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    // Define default options for Highcharts caption properties.
    $options['align'] = ['default' => 'left'];
    $options['floating'] = ['default' => FALSE];
    $options['margin'] = ['default' => 15];
    $options['useHTML'] = ['default' => TRUE];
    $options['verticalAlign'] = ['default' => 'bottom'];
    $options['x'] = ['default' => 0];
    $options['y'] = ['default' => 0];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['align'] = [
      '#type' => 'select',
      '#title' => $this->t('Align'),
      '#options' => [
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right'),
      ],
      '#default_value' => $this->options['align'],
      '#description' => $this->t('Specifies the horizontal alignment of the caption.'),
    ];

    $form['floating'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Floating'),
      '#default_value' => $this->options['floating'],
      '#description' => $this->t('If set to true, the caption will float above the chart.'),
    ];

    $form['margin'] = [
      '#type' => 'number',
      '#title' => $this->t('Margin'),
      '#default_value' => $this->options['margin'],
      '#description' => $this->t('The margin between the caption and the plot area.'),
    ];

    $form['useHTML'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use HTML'),
      '#default_value' => $this->options['useHTML'],
      '#description' => $this->t('Whether to use HTML to render the text of the caption.'),
    ];

    $form['verticalAlign'] = [
      '#type' => 'select',
      '#title' => $this->t('Vertical Align'),
      '#options' => [
        'top' => $this->t('Top'),
        'middle' => $this->t('Middle'),
        'bottom' => $this->t('Bottom'),
      ],
      '#default_value' => $this->options['verticalAlign'],
      '#description' => $this->t('Specifies the vertical alignment of the caption.'),
    ];

    $form['x'] = [
      '#type' => 'number',
      '#title' => $this->t('X'),
      '#default_value' => $this->options['x'],
      '#description' => $this->t('The x offset of the caption relative to its alignment.'),
    ];

    $form['y'] = [
      '#type' => 'number',
      '#title' => $this->t('Y'),
      '#default_value' => $this->options['y'],
      '#description' => $this->t('The y offset of the caption relative to its vertical alignment.'),
    ];
  }

}
